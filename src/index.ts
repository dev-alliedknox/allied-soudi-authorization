import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import routes from './routes';
import { errorHandler } from 'allied-kernel';
import * as awsXRay from 'aws-xray-sdk';
import { userValidation, userUpdateValidation, groupValidation, groupUpdateValidation,
    profileValidation, profileUpdateValidation} from './middlewares/validation-middleware';
const serverless = require('serverless-http');
const app = express();

app.use(bodyParser.json());
app.use(compression());
app.use(awsXRay.express.openSegment('soudiAuthorization'));
app.post('/users', userValidation);
app.put('/users/:id', userUpdateValidation);
app.post('/groups', groupValidation);
app.put('/groups/:id', groupUpdateValidation);
app.post('/profiles', profileValidation);
app.put('/profiles/:id', profileUpdateValidation);
app.use(routes);
app.use(awsXRay.express.closeSegment());
app.use(errorHandler);

module.exports.handler = serverless(express().use('/authorization', app));