import { Router } from 'express';
import { asyncMiddleware } from 'allied-kernel';
import * as userController from '../controllers/user-controller';
import * as groupController from '../controllers/group-controller';
import * as profileController from '../controllers/profile-controller';
import * as permissionController from '../controllers/permission-controller';
import * as cors from 'cors';
import { swaggerSetup, swaggerUi } from '../configs/swagger';

const router = Router();

router.options('*', cors());

router.use('/', swaggerUi.serve);
router.get('/api-docs', swaggerSetup);

router.get('/resellers', cors(), asyncMiddleware(userController.listResellers));
router.get('/users', cors(), asyncMiddleware(userController.listAll));
router.get('/users/:id', cors(), asyncMiddleware(userController.listById));
router.get('/users/:id/resellers', cors(), asyncMiddleware(userController.listResellersByUserId));
router.post('/users', cors(), asyncMiddleware(userController.create));
router.delete('/users/:id', cors(), asyncMiddleware(userController.remove));
router.delete('/users/resellers/disassociate', cors(), asyncMiddleware(userController.disassociateResellers));
router.put('/users/:id', cors(), asyncMiddleware(userController.update));

router.get('/groups', cors(), asyncMiddleware(groupController.listAll));
router.get('/groups/:id', cors(), asyncMiddleware(groupController.listById));
router.get('/groups/profiles/:idProfile', cors(), asyncMiddleware(groupController.listByIdProfile));
router.post('/groups/', cors(), asyncMiddleware(groupController.create));
router.delete('/groups/:id', cors(), asyncMiddleware(groupController.remove));
router.put('/groups/:id', cors(), asyncMiddleware(groupController.update));
router.get('/groups/users/:idGroup', cors(), asyncMiddleware(groupController.usersByGroup));
router.post('/groups/users/associate', cors(), asyncMiddleware(groupController.associateUser));
router.delete('/groups/users/disassociate', cors(), asyncMiddleware(groupController.disassociateUser));

router.get('/profiles', cors(), asyncMiddleware(profileController.listAll));
router.get('/profiles/:id', cors(), asyncMiddleware(profileController.listById));
router.post('/profiles/', cors(), asyncMiddleware(profileController.create));
router.delete('/profiles/:id', cors(), asyncMiddleware(profileController.remove));
router.put('/profiles/:id', cors(), asyncMiddleware(profileController.update));
router.get('/profiles/permissions/:id', cors(), asyncMiddleware(profileController.listPermissionsByProfile));
router.post('/profiles/permissions/associate', cors(), asyncMiddleware(profileController.associatePermissions));
router.delete('/profiles/permissions/disassociate', cors(), asyncMiddleware(profileController.disassociatePermissions));
router.get('/profiles/groups/:id', cors(), asyncMiddleware(profileController.listGroupsByProfile));
router.post('/profiles/groups/associate', cors(), asyncMiddleware(profileController.associateGroups));
router.delete('/profiles/groups/disassociate', cors(), asyncMiddleware(profileController.disassociateGroups));

router.get('/permissions', cors(), asyncMiddleware(permissionController.listAll));

export default router;