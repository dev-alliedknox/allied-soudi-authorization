
interface IPermission {

    id: number;
    resource: string;
}

export { IPermission };