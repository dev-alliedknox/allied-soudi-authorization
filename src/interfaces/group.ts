
interface IGroup {

    id: number;
    name: string;
    description: string;
}

export { IGroup };