
interface IAssociation {

    idGroup: number;
    idUser: number;
}

export { IAssociation };