
interface IProfilePermissionDisassociation {

    idPermission: number;
    idProfile: number;
}

export { IProfilePermissionDisassociation };