
interface IProfileGroupAssociation {

    idGroup: number;
    idProfile: number;
}

export { IProfileGroupAssociation };