
interface IProfile {

    id: number;
    name: string;
}

export { IProfile };