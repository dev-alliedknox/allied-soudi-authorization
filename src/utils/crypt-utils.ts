import * as bcrypt from 'bcryptjs';

export const encrypt = (password: string): string => bcrypt.hashSync(password, 10);