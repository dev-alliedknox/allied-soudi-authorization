
export const isUndefinedOrNull = (data: any): boolean => {
    
    return data == undefined || data == null;
}