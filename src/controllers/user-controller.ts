import { Request, Response } from 'express';
import { UserClientDB } from '../clients/user-client-db';
import { UserService } from '../services/user-service';
import { User } from '../models/user';
import { IUser } from '../interfaces/user';

export const listAll = async (req: Request, res: Response): Promise<any> => {

    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);
    let users: IUser[] = await userService.listAll();

    return res.send({ users: users });
}

export const listById = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);

    let user: IUser = await userService.listById(id);

    return res.send(user);
}

export const create = async (req: Request, res: Response): Promise<any> => {

    let user = new User({ ...req.body.user });
    let resellers: number[] = req.body.resellers;

    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);

    await userService.create(user, resellers);

    return res.status(201).end();
}

export const remove = async (req: Request, res: Response): Promise<any> => {

    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);
    let id: number = Number(req.params.id);

    await userService.remove(id);

    res.status(204).end();
}

export const update = async (req: Request, res: Response): Promise<any> => {

    let id: number = Number(req.params.id);
    let user = new User({ ...req.body.user });
    let resellers: number[] = req.body.resellers;

    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);

    await userService.update(id, user, resellers);

    res.status(204).end();
}

export const listResellers = async (req: Request, res: Response): Promise<any> => {

    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);
    let resellers = await userService.listResellers();

    return res.send({ resellers: resellers });
}

export const listResellersByUserId = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);
    let resellers = await userService.listResellersByUserId(id);

    return res.send({ resellers: resellers });
}

export const disassociateResellers = async (req: Request, res: Response): Promise<any> => {

    let userResellers: any[] = req.body.userResellers;
    
    const userClientDB = new UserClientDB();
    const userService = new UserService(userClientDB);

    await userService.disassociateResellers(userResellers);

    res.status(204).end();
}