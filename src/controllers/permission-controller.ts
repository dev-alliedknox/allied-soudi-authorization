import { Request, Response } from 'express';
import { IPermission } from '../interfaces/permission';
import { PermissionClientDB } from '../clients/permission-client-db';
import { PermissionService } from '../services/permission-service';


export const listAll = async (req: Request, res: Response): Promise<any> => {

    const permissionClientDB = new PermissionClientDB();
    const permissionService = new PermissionService(permissionClientDB);
    let permissions: IPermission[] = await permissionService.listAll();

    return res.send({ permissions: permissions });
}