import { Request, Response } from 'express';
import { GroupClientDB } from '../clients/group-client-db';
import { GroupService } from '../services/group-service';
import { IGroup } from '../interfaces/group';
import { Group } from '../models/group';
import { IAssociation } from '../interfaces/association';

export const listAll = async (req: Request, res: Response): Promise<any> => {

    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);
    
    let groups: IGroup[] = await groupService.listAll();

    return res.send({ groups: groups });
}

export const listById = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);

    let group: IGroup = await groupService.listById(id);

    return res.send(group);
}

export const create = async (req: Request, res: Response): Promise<any> => {

    let group = new Group({ ...req.body.group });
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);

    let id = await groupService.create(group);

    return res.status(201).send({ groupId: id[0] });
}

export const remove = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);

    await groupService.remove(id);

    res.status(204).end();
}

export const update = async (req: Request, res: Response): Promise<any> => {

    let id: number = Number(req.params.id);
    let group = new Group({ ...req.body.group });
    
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);

    await groupService.update(id, group);

    res.status(204).end();
}

export const associateUser = async (req: Request, res: Response): Promise<any> => {

    let associations: IAssociation[] = req.body.associations;
    
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);

    await groupService.associateUser(associations);

    res.status(201).end();
}

export const disassociateUser = async (req: Request, res: Response): Promise<any> => {

    let associations: IAssociation[] = req.body.associations;
    
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);

    await groupService.disassociateUser(associations);

    res.status(204).end();
}

export const listByIdProfile = async (req: Request, res: Response): Promise<any> => {

    let idProfile = Number(req.params.idProfile);
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);

    let groups: IGroup[] = await groupService.listByIdProfile(idProfile);

    return res.send({ groups: groups });
}

export const usersByGroup = async (req: Request, res: Response): Promise<any> => {
    let id = Number(req.params.idGroup);
    console.log('REQ-data: ', id)
    const groupClientDB = new GroupClientDB();
    const groupService = new GroupService(groupClientDB);
    
    let users = await groupService.usersByGroup(id);

    return res.send({users: users});
}