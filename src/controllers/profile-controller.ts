import { Request, Response } from 'express';
import { ProfileClientDB } from '../clients/profile-client-db';
import { ProfileService } from '../services/profile-service';
import { IProfile } from '../interfaces/profile';
import { Profile } from '../models/profile';
import { ProfilePermissionAssociation } from '../models/profile-permission-association';
import { IProfilePermissionDisassociation } from '../interfaces/profile-permission-disassociation';
import { IProfileGroupAssociation } from '../interfaces/profile-group-association';
import { IGroup } from '../interfaces/group';

export const listAll = async (req: Request, res: Response): Promise<any> => {

    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);
    let profiles: IProfile[] = await profileService.listAll();

    return res.send({ profiles: profiles });
}

export const listById = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    let profile: IProfile = await profileService.listById(id);

    return res.send(profile);
}

export const create = async (req: Request, res: Response): Promise<any> => {

    let profile = new Profile({ ...req.body.profile });
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    let id = await profileService.create(profile);

    return res.status(201).send({ profileId: id[0] });
}

export const remove = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    await profileService.remove(id);

    res.status(204).end();
}

export const update = async (req: Request, res: Response): Promise<any> => {

    let id: number = Number(req.params.id);
    let profile = new Profile({ ...req.body.profile });
    
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    await profileService.update(id, profile);

    res.status(204).end();
}

export const associatePermissions = async (req: Request, res: Response): Promise<any> => {

    let permissionAssociations: ProfilePermissionAssociation[] = req.body.permissionAssociations;
    
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    await profileService.associatePermissions(permissionAssociations);

    res.status(201).end();
}

export const disassociatePermissions = async (req: Request, res: Response): Promise<any> => {

    let profilePermissions: IProfilePermissionDisassociation[] = req.body.profilePermissions;
    
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    await profileService.disassociatePermissions(profilePermissions);

    res.status(204).end();
}

export const associateGroups = async (req: Request, res: Response): Promise<any> => {

    let permissionGroups: IProfileGroupAssociation[] = req.body.permissionGroups;
    
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    await profileService.associateGroups(permissionGroups);

    res.status(201).end();
}

export const disassociateGroups = async (req: Request, res: Response): Promise<any> => {

    let profileGroups: IProfileGroupAssociation[] = req.body.profileGroups;
    
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    await profileService.disassociateGroups(profileGroups);

    res.status(204).end();
}

export const listPermissionsByProfile = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    let permissions: any = await profileService.listPermissionsByProfile(id);

    return res.send({permissions: permissions});
}

export const listGroupsByProfile = async (req: Request, res: Response): Promise<any> => {

    let id = Number(req.params.id);
    const profileClientDB = new ProfileClientDB();
    const profileService = new ProfileService(profileClientDB);

    let groups = await profileService.listGroupsByProfile(id);

    return res.send({groups: groups});
}