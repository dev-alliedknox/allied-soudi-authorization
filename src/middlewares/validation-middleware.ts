import { Request, Response, NextFunction } from 'express';
import { ValidationError } from '../errors/validation-error';
import { isUndefinedOrNull } from '../utils/utils';

export const userValidation = (req: Request, res: Response, next: NextFunction): void => {

    if (isUndefinedOrNull(req.body.user)) throw new ValidationError(`Campo 'user' é obrigátorio.`, 400);
    let length = Object.keys(req.body.user).length;
    
    if (isUndefinedOrNull(req.body.user.name)) throw new ValidationError(`Campo 'name' é obrigátorio.`, 400);
    if (isUndefinedOrNull(req.body.user.email)) throw new ValidationError(`Campo 'email' é obrigátorio.`, 400);
    if (isUndefinedOrNull(req.body.user.isActive)) throw new ValidationError(`Campo 'isActive' é obrigátorio.`, 400);
    if (isUndefinedOrNull(req.body.user.password)) throw new ValidationError(`Campo 'password' é obrigátorio.`, 400);
    if(length != 4) throw new ValidationError(`Campos desconhecidos no request.`, 400);

    next();
}

export const userUpdateValidation = (req: Request, res: Response, next: NextFunction): void => {

    if (isUndefinedOrNull(req.body.user)) throw new ValidationError(`Campo 'user' é obrigátorio.`, 400);
    let length = Object.keys(req.body.user).length;

    if(length <= 0) throw new ValidationError(`Nenhum campo informado para alteração.`, 400);

    next();
}

export const groupValidation = (req: Request, res: Response, next: NextFunction): void => {

    if (isUndefinedOrNull(req.body.group)) throw new ValidationError(`Campo 'group' é obrigátorio.`, 400);
    let length = Object.keys(req.body.group).length;
    
    if (isUndefinedOrNull(req.body.group.name)) throw new ValidationError(`Campo 'name' é obrigátorio.`, 400);
    if (isUndefinedOrNull(req.body.group.description)) throw new ValidationError(`Campo 'description' é obrigátorio.`, 400);

    if(length != 2) throw new ValidationError(`Campos desconhecidos no request.`, 400);

    next();
}

export const groupUpdateValidation = (req: Request, res: Response, next: NextFunction): void => {

    if (isUndefinedOrNull(req.body.group)) throw new ValidationError(`Campo 'group' é obrigátorio.`, 400);
    let length = Object.keys(req.body.group).length;
    let body = req.body.group;

    if(length <= 0) throw new ValidationError(`Nenhum campo informado para alteração.`, 400);
    if(length > 2 || !body.name && !body.description) throw new ValidationError(`Campos desconhecidos no request.`, 400);

    next();
}

export const profileValidation = (req: Request, res: Response, next: NextFunction): void => {

    if (isUndefinedOrNull(req.body.profile)) throw new ValidationError(`Campo 'profile' é obrigátorio.`, 400);
    let length = Object.keys(req.body.profile).length;
    
    if (isUndefinedOrNull(req.body.profile.name)) throw new ValidationError(`Campo 'name' é obrigátorio.`, 400);

    if(length != 1) throw new ValidationError(`Campos desconhecidos no request.`, 400);

    next();
}

export const profileUpdateValidation = (req: Request, res: Response, next: NextFunction): void => {

    if (isUndefinedOrNull(req.body.profile)) throw new ValidationError(`Campo 'profile' é obrigátorio.`, 400);
    let length = Object.keys(req.body.profile).length;
    let body = req.body.profile;

    if(length <= 0) throw new ValidationError(`Nenhum campo informado para alteração.`, 400);
    if(length > 1 || !body.name) throw new ValidationError(`Campos desconhecidos no request.`, 400);

    next();
}
