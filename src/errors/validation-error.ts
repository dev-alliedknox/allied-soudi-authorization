import { Err } from 'allied-kernel';

class ValidationError extends Err {

    public constructor(message: string, statusCode: number) {

        super(message, statusCode, 'validation-error');
    }

} 

export { ValidationError };