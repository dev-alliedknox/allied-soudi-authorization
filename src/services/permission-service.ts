import { PermissionClientDB } from '../clients/permission-client-db';
import { IPermission } from '../interfaces/permission';

class PermissionService {

    private permissionClientDB: PermissionClientDB;

    public constructor(permissionClientDB: PermissionClientDB) {

        this.permissionClientDB = permissionClientDB;
    }

    public async listAll(): Promise<IPermission[]> {

        return await this.permissionClientDB.listAll();
    }
}

export { PermissionService };