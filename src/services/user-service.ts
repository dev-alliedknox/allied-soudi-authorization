import { UserClientDB } from '../clients/user-client-db';
import { User } from '../models/user';
import { IUser } from '../interfaces/user';
import { Err } from 'allied-kernel';

class UserService {

    private userClientDB: UserClientDB;

    public constructor(userClientDB: UserClientDB) {

        this.userClientDB = userClientDB;
    }

    public async listAll(): Promise<IUser[]> {

        let users: any = await this.userClientDB.listAll();
        users.map((user): IUser => { delete user.password; return user });

        return users;
    }

    public async listById(id: number): Promise<IUser> {

        let users: IUser[] = await this.userClientDB.listById(id);

        if (users.length === 0) throw new Err('Usuário não encontrado', 404, 'not-found-error');

        return users[0];
    }

    public async create(user: User, resellers: number[]): Promise<void> {

        return this.userClientDB.create(user, resellers);
    }

    public async remove(id: number): Promise<void> {

        await this.listById(id);
        
        return this.userClientDB.remove(id);
    }

    public async update(id: number, user: User, resellers: number[]): Promise<void> {

        await this.listById(id);

        return this.userClientDB.update(id, user, resellers);
    }

    public async listResellers(): Promise<any> {

        let resellers = await this.userClientDB.listResellers();

        return resellers;
    }

    public async listResellersByUserId(idUser: number): Promise<any> {

        let resellers = await this.userClientDB.listResellersByUserId(idUser);

        return resellers;
    }

    public async disassociateResellers(userResellers: any): Promise<any> {

        let promises = userResellers.map(async (userReseller): Promise<void> => 
            this.userClientDB.disassociateResellers(userReseller));

        await Promise.all(promises);
    }
}

export { UserService };