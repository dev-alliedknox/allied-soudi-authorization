import { GroupClientDB } from '../clients/group-client-db';
import { IGroup } from '../interfaces/group';
import { Err } from 'allied-kernel';
import { Group } from '../models/group';
import { IAssociation } from '../interfaces/association';

class GroupService {

    private groupClientDB: GroupClientDB;

    public constructor(groupClientDB: GroupClientDB) {

        this.groupClientDB = groupClientDB;
    }

    public async listAll(): Promise<IGroup[]> {

        return this.groupClientDB.listAll();
    }

    public async listById(id: number): Promise<IGroup> {

        let groups: IGroup[] = await this.groupClientDB.listById(id);

        if (groups.length === 0) throw new Err('Grupo não encontrado', 404, 'not-found-error');

        return groups[0];
    }

    public async create(group: Group): Promise<number[]> {

        return this.groupClientDB.create(group);
    }

    public async remove(id: number): Promise<void> {

        await this.listById(id);
        
        return this.groupClientDB.remove(id);
    }

    public async update(id: number, group: Group): Promise<void> {

        await this.listById(id);

        return this.groupClientDB.update(id, group);
    }

    public async associateUser(associations: IAssociation[]): Promise<void> {

        associations.map(async (association): Promise<void> => {

            await this.groupClientDB.associateUser(association);
        });
    }

    public async disassociateUser(associations: IAssociation[]): Promise<void> {

        const promises = associations.map((association): Promise<void> => this.groupClientDB.disassociateUser(association));
        await Promise.all(promises);
    }

    public async listByIdProfile(idProfile: number): Promise<IGroup[]> {

        let groups: IGroup[] = await this.groupClientDB.listByIdProfile(idProfile);

        return groups;
    }

    public async usersByGroup(id): Promise<any> {

        let users = await this.groupClientDB.usersByGroup(id);
        
        return users;
    }

}

export { GroupService };