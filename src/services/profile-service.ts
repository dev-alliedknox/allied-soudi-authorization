import { ProfileClientDB } from '../clients/profile-client-db';
import { IProfile } from '../interfaces/profile';
import { Err } from 'allied-kernel';
import { Profile } from '../models/profile';
import { ProfilePermissionAssociation } from '../models/profile-permission-association';
import { IProfilePermissionDisassociation } from '../interfaces/profile-permission-disassociation';
import { IProfileGroupAssociation } from '../interfaces/profile-group-association';
import { ValidationError } from '../errors/validation-error';

class ProfileService {

    private profileClientDB: ProfileClientDB;

    public constructor(profileClientDB: ProfileClientDB) {

        this.profileClientDB = profileClientDB;
    }

    public async listAll(): Promise<IProfile[]> {

        return this.profileClientDB.listAll();
    }

    public async listById(id: number): Promise<IProfile> {

        let profiles: IProfile[] = await this.profileClientDB.listById(id);

        if (profiles.length === 0) throw new Err('Perfil não encontrado', 404, 'not-found-error');

        return profiles[0];
    }

    public async create(profile: Profile): Promise<number[]> {

        return this.profileClientDB.create(profile);
    }

    public async remove(id: number): Promise<void> {

        await this.listById(id);

        let promises = await Promise.all([ 
            this.profileClientDB.getProfilePermissionsByIdProfile(id),
            this.profileClientDB.getProfileGroupsByIdProfile(id) 
        ]);

        let data = promises.reduce((acc, val): any => acc.concat(val), []);

        if (data.length > 0) 
            throw new ValidationError('Não é possível excluir perfil com grupos ou permissões associadas.', 422);
        
        return this.profileClientDB.remove(id);
    }

    public async update(id: number, profile: Profile): Promise<void> {

        await this.listById(id);
        await this.profileClientDB.deletePermissions(id);

        return this.profileClientDB.update(id, profile);
    }

    public async associatePermissions(permissionAssociations: ProfilePermissionAssociation[]): Promise<void> {

        let promises = permissionAssociations.map(async (association): Promise<void> => 
            this.profileClientDB.associatePermissions(association));

        await Promise.all(promises);
    }

    public async disassociatePermissions(profilePermissions: IProfilePermissionDisassociation[]): Promise<void> {

        let promises = profilePermissions.map(async (profilePermission): Promise<void> => 
            this.profileClientDB.disassociatePermissions(profilePermission));

        await Promise.all(promises);
    }

    public async associateGroups(permissionGroups: IProfileGroupAssociation[]): Promise<void> {

        let promises = permissionGroups.map(async (permissionGroups): Promise<void> => 
            this.profileClientDB.associateGroups(permissionGroups));

        await Promise.all(promises);
    }

    public async disassociateGroups(profileGroups: IProfileGroupAssociation[]): Promise<void> {

        let promises = profileGroups.map(async (profileGroup): Promise<void> => 
            this.profileClientDB.disassociateGroups(profileGroup));

        await Promise.all(promises);
    }

    public async listPermissionsByProfile(id: number): Promise<IProfile> {

        let permissions = await this.profileClientDB.listPermissionsByProfile(id);
        // if (profiles.length === 0) throw new Err('Perfil não encontrado', 404, 'not-found-error');

        return permissions;
    }

    public async listGroupsByProfile(id: number): Promise<IProfile> {

        let groups = await this.profileClientDB.listGroupsByProfile(id);
        // if (profiles.length === 0) throw new Err('Perfil não encontrado', 404, 'not-found-error');

        return groups;
    }

}

export { ProfileService };