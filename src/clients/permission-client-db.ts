import { KnexSingleton } from "../utils/knexSingleton";
import { IPermission } from "../interfaces/permission";

class PermissionClientDB {

    private conn = KnexSingleton.getInstance(
        process.env.DBHOST,
        process.env.DBUSER,
        process.env.DBPASSWORD, 
        process.env.DBNAME).conn;

    public constructor() {}

    public async listAll(): Promise<IPermission[]> {

        return this.conn.select('*').from('permissions');
    }
}

export { PermissionClientDB };