import { KnexSingleton } from '../utils/knexSingleton';
import { IProfile } from '../interfaces/profile';
import { Profile } from '../models/profile';
import { ProfilePermissionAssociation } from '../models/profile-permission-association';
import { IProfilePermissionDisassociation } from '../interfaces/profile-permission-disassociation';
import { IProfileGroupAssociation } from '../interfaces/profile-group-association';

class ProfileClientDB {

    private conn = KnexSingleton.getInstance(
        process.env.DBHOST,
        process.env.DBUSER,
        process.env.DBPASSWORD, 
        process.env.DBNAME).conn;

    public constructor() {}

    public async listAll(): Promise<IProfile[]> {

        return this.conn.select('*').from('profiles');
    }

    public async listById(id: number): Promise<IProfile[]> {

        return this.conn.select('*')
            .from('profiles')
            .where('id', id);
    }

    public async create(profile: Profile): Promise<number[]> {

        return this.conn('profiles').insert(profile).returning('id');
    }

    public async remove(id: number): Promise<void> {

        return this.conn.delete()
            .from('profiles')
            .where('id', id);
    }

    public async update(id: number, profile: Profile): Promise<void> {

        return this.conn.update(profile)
            .from('profiles')
            .where('id', id);
    }

    public async associatePermissions(permissionAssociations: ProfilePermissionAssociation): Promise<void> {

        return this.conn('profilePermission').insert(permissionAssociations);
    }

    public async disassociatePermissions(profilePermission: IProfilePermissionDisassociation): Promise<void> {

        return this.conn.delete()
            .from('profilePermission')
            .where('idProfile', profilePermission.idProfile)
            .where('idPermission', profilePermission.idPermission);
    }

    public async associateGroups(permissionGroups: IProfileGroupAssociation): Promise<void> {

        return this.conn('profileGroup').insert(permissionGroups);
    }

    public async disassociateGroups(profileGroup: IProfileGroupAssociation): Promise<void> {

        return this.conn.delete()
            .from('profileGroup')
            .where('idProfile', profileGroup.idProfile)
            .where('idGroup', profileGroup.idGroup);
    }

    public async listPermissionsByProfile(idProfile: number): Promise<any> {

        return this.conn.
            select(this.conn.raw("permissions.id, permissions.resource,profilePermission.canRead,profilePermission.canWrite"))
            .from(this.conn.raw("permissions, profilePermission"))
            .whereRaw(`profilePermission.idProfile = ${idProfile}`)
            .andWhereRaw("profilePermission.idPermission = permissions.id");    
    }

    public async listGroupsByProfile(id: number): Promise<any> {

        return this.conn.
            select(this.conn.raw("groups.id, groups.name, groups.description"))
            .from(this.conn.raw("groups, profileGroup"))
            .whereRaw(`profileGroup.idProfile = ${id}`)
            .andWhereRaw("profileGroup.idGroup = groups.id");    
    }


    public async getProfilePermissionsByIdProfile(idProfile: number): Promise<IProfile[]> {

        return this.conn.select('*')
            .from('profiles')
            .innerJoin('profilePermission', 'profiles.id', 'profilePermission.idProfile')
            .innerJoin('permissions', 'profilePermission.idPermission', 'permissions.id')
            .where('profiles.id', idProfile);
    }

    public async getProfileGroupsByIdProfile(idProfile: number): Promise<IProfile[]> {

        return this.conn.select('*')
            .from('profiles')
            .innerJoin('profileGroup', 'profiles.id', 'profileGroup.idProfile')
            .innerJoin('groups', 'profileGroup.idProfile', 'groups.id')
            .where('profiles.id', idProfile);
    }

    public async deletePermissions(idProfile: number): Promise<void> {

        return this.conn.delete()
            .from('profilePermission')
            .where('idProfile', idProfile);
    }
}

export { ProfileClientDB }