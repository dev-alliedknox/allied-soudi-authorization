import { KnexSingleton } from '../utils/knexSingleton';
import { User } from '../models/user';
import { IUser } from '../interfaces/user';

class UserClientDB {

    private conn = KnexSingleton.getInstance(
        process.env.DBHOST,
        process.env.DBUSER,
        process.env.DBPASSWORD, 
        process.env.DBNAME).conn;

    public constructor() {}

    public async create(user: User, resellers: number[]): Promise<void> {

        return this.conn.transaction((trx): any => {

            return trx.
                insert(user).into('knoxUser')
                .then((ids): any => {
                    const payload = resellers.map((reseller): any => { return { idReseller: reseller, idUser: ids[0] }});

                    return trx('userReseller').insert(payload);
                });
        });
    }

    public async remove(id: number): Promise<void> {

        return this.conn.delete()
            .from('userReseller')
            .where('idUser', id)
            .then(async (): Promise<void> => {
                await this.conn.delete().from('knoxUser').where('id', id)
                await this.conn.delete().from('userGroup').where('idUser', id)
            });
    }

    public async update(id: number, user: User, resellers: number[]): Promise<void> {

        return this.conn.transaction((trx): any => {

            return trx.
                update(user).from('knoxUser').where('id', id)
                .then((): any => {
                    const payload = resellers.map((reseller): any => { return { idReseller: reseller, idUser: id }});

                    return trx('userReseller').insert(payload);
                });
        });
    }

    public async listAll(): Promise<IUser[]> {

        return this.conn.select('*').from('knoxUser');
    }

    public async listById(id: number): Promise<IUser[]> {

        return this.conn.select('id', 'name', 'email', 'isActive')
            .from('knoxUser')
            .where('id', id);
    }

    public async listResellers(): Promise<any> {

        return this.conn.select('idempresa', 'nome_fantasia as nomeFantasia').from('linxResellers');
    }

    public async listResellersByUserId(idUser: number): Promise<any> {

        return this.conn.select('idempresa', 'nome_fantasia as nomeFantasia')
            .from('linxResellers')
            .innerJoin('userReseller', 'linxResellers.idempresa', 'userReseller.idReseller')
            .innerJoin('knoxUser', 'userReseller.idUser', 'knoxUser.id')
            .where('knoxUser.id', idUser);
    }

    public deleteResellers(idUser): Promise<any> {

        return this.conn.delete()
            .from('userReseller')
            .where('idUser', idUser);
    }

    public async disassociateResellers(userReseller: any): Promise<void> {

        return this.conn.delete()
            .from('userReseller')
            .where('idUser', userReseller.idUser)
            .andWhere('idReseller', userReseller.idReseller);
    }
}

export { UserClientDB }