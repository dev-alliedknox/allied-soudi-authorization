import { KnexSingleton } from '../utils/knexSingleton';
import { IGroup } from '../interfaces/group';
import { Group } from '../models/group';
import { IAssociation } from '../interfaces/association';

class GroupClientDB {

    private conn = KnexSingleton.getInstance(
        process.env.DBHOST,
        process.env.DBUSER,
        process.env.DBPASSWORD, 
        process.env.DBNAME).conn;

    public constructor() {}

    public async listAll(): Promise<IGroup[]> {

        return this.conn.select('*').from('groups');
    }

    public async listById(id: number): Promise<IGroup[]> {

        return this.conn.select('*')
            .from('groups')
            .where('id', id);
    }

    public async create(group: Group): Promise<number[]> {

        return this.conn('groups').insert(group).returning('id');
    }

    public async remove(id: number): Promise<void> {

        return this.conn.delete()
            .from('groups')
            .where('id', id);
    }

    public async update(id: number, group: Group): Promise<void> {

        return this.conn.update(group)
            .from('groups')
            .where('id', id);
    }

    public async associateUser(association: IAssociation): Promise<void> {

        return this.conn('userGroup').insert(association);
    }

    public async disassociateUser(association: IAssociation): Promise<void> {

        return this.conn.delete()
            .from('userGroup')
            .where('idGroup', association.idGroup)
            .andWhere('idUser', association.idUser);
    }

    public listByIdProfile(id: number): Promise<any> {

        return this.conn.select('groups.*')
            .from('groups')
            .innerJoin('profileGroup', 'groups.id', 'profileGroup.idGroup')
            .innerJoin('profiles', 'profileGroup.idProfile', 'profiles.id')
            .where('profiles.id', id);
    }

    public async usersByGroup(id): Promise<any> {
        let idGroup = id;
        return this.conn.
                select(this.conn.raw("knoxUser.name, knoxUser.id"))
                .from(this.conn.raw("userGroup, knoxUser"))
                .whereRaw(`userGroup.idGroup = ${idGroup}`)
                .andWhereRaw("userGroup.idUser = knoxUser.id");
    }

}

export { GroupClientDB }