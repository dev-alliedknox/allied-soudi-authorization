import { encrypt } from "../utils/crypt-utils";

class User {

    private name: string;
    private email: string;
    private isActive: boolean;
    private password: string;

    public constructor({ name, email, isActive, password }) {

        this.name = name;
        this.email = email;
        this.isActive = isActive;
        if (password != undefined) this.password = encrypt(password);
    }
}

export { User };