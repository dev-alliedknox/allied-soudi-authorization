
class Group {

    private name: string;
    private description: string;

    public constructor({ name, description }) {

        this.name = name;
        this.description = description;
    }
}

export { Group };