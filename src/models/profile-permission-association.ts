
class ProfilePermissionAssociation {

    private idPermission: number;
    private idProfile: number;
    private canRead: boolean;
    private canWrite: boolean;

    public constructor({ idPermission, idProfile, canRead, canWrite }) {

        this.idPermission = idPermission;
        this.idProfile = idProfile;
        this.canRead = canRead;
        this.canWrite = canWrite;
    }

}

export { ProfilePermissionAssociation };